(fn love.load []
 (love.window.setMode 0 0
 	{:resizable true
	 :vsync 0})
 (set posX 0)
 (set posY 0))

(fn love.update [dt]
 (local movSpd 1)
 (if (love.keyboard.isDown "left") (set posX (- posX (* (+ dt 1) movSpd)))
	  (love.keyboard.isDown "right") (set posX (+ posX (* (+ dt 1) movSpd)))
	  (love.keyboard.isDown "up") (set posY (- posY (* (+ dt 1) movSpd)))
	  (love.keyboard.isDown "down") (set posY (+ posY (* (+ dt 1) movSpd)))
	  ))

(fn love.draw []
 (love.graphics.setColor 0 1 0)
 (love.graphics.circle "fill" posX posY 100)

 ;; Temp
 (love.graphics.setColor 1 1 1)
 (love.graphics.print
  (.. "width: " (love.graphics.getWidth)
  		"\nheight :" (love.graphics.getHeight)
		"\nposX: " posX)))
