love.load = function()
  love.window.setMode(0, 0, {resizable = true, vsync = 0})
  posX = 0
  posY = 0
  return nil
end
love.update = function(dt)
  local movSpd = 1
  if love.keyboard.isDown("left") then
    posX = (posX - ((dt + 1) * movSpd))
    return nil
  elseif love.keyboard.isDown("right") then
    posX = (posX + ((dt + 1) * movSpd))
    return nil
  elseif love.keyboard.isDown("up") then
    posY = (posY - ((dt + 1) * movSpd))
    return nil
  elseif love.keyboard.isDown("down") then
    posY = (posY + ((dt + 1) * movSpd))
    return nil
  else
    return nil
  end
end
love.draw = function()
  love.graphics.setColor(0, 1, 0)
  love.graphics.circle("fill", posX, posY, 100)
  love.graphics.setColor(1, 1, 1)
  return love.graphics.print(("width: " .. love.graphics.getWidth() .. "\nheight :" .. love.graphics.getHeight() .. "\nposX: " .. posX))
end
return love.draw
